package com.tasks.application.services;

import com.tasks.domain.model.Task;
import com.tasks.domain.ports.in.CreateTaskUseCase;
import com.tasks.domain.ports.in.GetTaskUseCase;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class TaskServiceTest {

    private TaskService taskService;
    private CreateTaskUseCase createTaskUseCaseMock;
    private GetTaskUseCase getTaskUseCaseMock;

    @Before
    public void setUp() {
        createTaskUseCaseMock = mock(CreateTaskUseCase.class);
        getTaskUseCaseMock = mock(GetTaskUseCase.class);
        taskService = new TaskService(createTaskUseCaseMock, getTaskUseCaseMock);
    }

    @Test
    public void testGetAllTasks() {
        Task task1 = new Task("1", "Task 1", "Description 1", null, false);
        Task task2 = new Task("2", "Task 2", "Description 2", null, true);
        List<Task> expectedTasks = Arrays.asList(task1, task2);
        when(getTaskUseCaseMock.getAllTasks()).thenReturn(expectedTasks);

        List<Task> result = taskService.getAllTasks();

        assertEquals(expectedTasks, result);
        verify(getTaskUseCaseMock, times(1)).getAllTasks();
    }

    @Test
    public void testGetTaskById() {
        String taskId = "1";
        Task expectedTask = new Task(taskId, "Task 1", "Description 1", null, false);
        when(getTaskUseCaseMock.getTaskById(taskId)).thenReturn(Optional.of(expectedTask));

        Optional<Task> result = taskService.getTaskById(taskId);

        assertEquals(Optional.of(expectedTask), result);
        verify(getTaskUseCaseMock, times(1)).getTaskById(taskId);
    }

    @Test
    public void testCreateTask() {
        Task task = new Task("1", "Tarea de ejemplo", "Descripción de la tarea", null, false);
        Task createdTask = new Task("1", "Tarea de ejemplo", "Descripción de la tarea", null, false);
        when(createTaskUseCaseMock.createTask(task)).thenReturn(createdTask);

        Task result = taskService.createTask(task);

        assertEquals(createdTask, result);
        verify(createTaskUseCaseMock, times(1)).createTask(task);
    }
}
