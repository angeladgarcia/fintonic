package com.tasks.application.usecases;

import com.tasks.domain.model.Task;
import com.tasks.domain.ports.out.TaskRepositoryPort;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class CreateTaskUseCaseImplTest {

    private CreateTaskUseCaseImpl createTaskUseCase;
    private TaskRepositoryPort taskRepositoryPortMock;

    @Before
    public void setUp() {
        taskRepositoryPortMock = mock(TaskRepositoryPort.class);
        createTaskUseCase = new CreateTaskUseCaseImpl(taskRepositoryPortMock);
    }

    @Test
    public void testCreateTask() {
        Task task = new Task("1", "Task 1", "Description 1", null, false);
        Task expectedTask = new Task("1", "Task 1", "Description 1", null, false);
        when(taskRepositoryPortMock.save(task)).thenReturn(expectedTask);

        Task result = createTaskUseCase.createTask(task);

        assertEquals(expectedTask, result);
        verify(taskRepositoryPortMock, times(1)).save(task);
    }
}
