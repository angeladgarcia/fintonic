package com.tasks.infraestructure.controllers;

import com.tasks.application.services.TaskService;
import com.tasks.domain.model.Task;
import com.tasks.infrastructure.controllers.TaskController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TaskControllerTest {

    @Mock
    private TaskService taskServiceMock;

    @InjectMocks
    private TaskController taskController;

    @BeforeEach
    public void setUp() {}

    @Test
    public void testCreateTask() {
        Task task = new Task("1", "Task 1", "Description 1", null, false);
        Task createdTask = new Task("1", "Task 1", "Description 1", null, false);
        when(taskServiceMock.createTask(task)).thenReturn(createdTask);

        ResponseEntity<Task> response = taskController.createTask(task);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(createdTask, response.getBody());
        verify(taskServiceMock, times(1)).createTask(task);
    }

    @Test
    public void testGetTaskById() {
        String taskId = "1";
        Task task = new Task(taskId, "Task 1", "Description 1", null, false);
        when(taskServiceMock.getTaskById(taskId)).thenReturn(Optional.of(task));

        ResponseEntity<Task> response = taskController.getTaskById(taskId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(task, response.getBody());
        verify(taskServiceMock, times(1)).getTaskById(taskId);
    }

    @Test
    public void testGetAllTasks() {
        Task task1 = new Task("1", "Task 1", "Description 1", null, false);
        Task task2 = new Task("2", "Task 2", "Description 2", null, true);
        List<Task> tasks = Arrays.asList(task1, task2);
        when(taskServiceMock.getAllTasks()).thenReturn(tasks);

        ResponseEntity<List<Task>> response = taskController.getAllTasks();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(tasks, response.getBody());
        verify(taskServiceMock, times(1)).getAllTasks();
    }
}
