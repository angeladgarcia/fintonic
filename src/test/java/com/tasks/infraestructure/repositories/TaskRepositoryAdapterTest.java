package com.tasks.infraestructure.repositories;

import com.tasks.domain.model.Task;
import com.tasks.infrastructure.entities.TaskEntity;
import com.tasks.infrastructure.repositories.TaskRepository;
import com.tasks.infrastructure.repositories.TaskRepositoryAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TaskRepositoryAdapterTest {

    @Mock
    private TaskRepository taskRepositoryMock;

    @InjectMocks
    private TaskRepositoryAdapter jpaTaskRepositoryAdapter;

    @BeforeEach
    public void setUp() {
        // No es necesario con MockitoExtension
    }

    @Test
    public void testSave() {
        // Arrange
        Task task = new Task("1", "Task 1", "Description 1", null, false);
        TaskEntity taskEntity = new TaskEntity("1", "Task 1", "Description 1", null, false);
        when(taskRepositoryMock.save(any(TaskEntity.class))).thenReturn(taskEntity);

        // Act
        Task result = jpaTaskRepositoryAdapter.save(task);

        // Assert
        assertEquals(task, result);
        verify(taskRepositoryMock, times(1)).save(any(TaskEntity.class));
    }

    @Test
    public void testFindById() {
        // Arrange
        String taskId = "1";
        Task task = new Task(taskId, "Task 1", "Description 1", null, false);
        TaskEntity taskEntity = new TaskEntity(taskId, "Task 1", "Description 1", null, false);
        when(taskRepositoryMock.findById(taskId)).thenReturn(Optional.of(taskEntity));

        // Act
        Optional<Task> result = jpaTaskRepositoryAdapter.findById(taskId);

        // Assert
        assertEquals(Optional.of(task), result);
        verify(taskRepositoryMock, times(1)).findById(taskId);
    }

    @Test
    public void testFindAll() {
        // Arrange
        TaskEntity taskEntity1 = new TaskEntity("1", "Task 1", "Description 1", null, false);
        TaskEntity taskEntity2 = new TaskEntity("2", "Task 2", "Description 2", null, true);
        List<TaskEntity> taskEntities = Arrays.asList(taskEntity1, taskEntity2);
        when(taskRepositoryMock.findAll()).thenReturn(taskEntities);

        // Act
        List<Task> result = jpaTaskRepositoryAdapter.findAll();

        // Assert
        assertEquals(2, result.size());
        assertEquals("1", result.get(0).getId());
        assertEquals("Task 1", result.get(0).getTitle());
        assertEquals("Description 1", result.get(0).getDescription());
        assertFalse(result.get(0).isCompleted());
        assertEquals("2", result.get(1).getId());
        assertEquals("Task 2", result.get(1).getTitle());
        assertEquals("Description 2", result.get(1).getDescription());
        assertTrue(result.get(1).isCompleted());
        verify(taskRepositoryMock, times(1)).findAll();
    }
}
