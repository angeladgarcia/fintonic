package com.tasks.domain.ports.out;

import com.tasks.domain.model.Task;

import java.util.List;
import java.util.Optional;

public interface TaskRepositoryPort {
    Task save(Task task);
    Optional<Task> findById(String id);
    List<Task> findAll();
}
