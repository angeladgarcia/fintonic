package com.tasks.domain.ports.in;

import com.tasks.domain.model.Task;

import java.util.List;
import java.util.Optional;

public interface GetTaskUseCase {
    Optional<Task> getTaskById(String id);
    List<Task> getAllTasks();
}
