package com.tasks.domain.ports.in;

import com.tasks.domain.model.Task;

public interface CreateTaskUseCase {
    Task createTask(Task task);
}
