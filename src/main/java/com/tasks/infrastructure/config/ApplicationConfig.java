package com.tasks.infrastructure.config;

import com.tasks.application.services.TaskService;
import com.tasks.domain.ports.in.CreateTaskUseCase;
import com.tasks.domain.ports.in.GetTaskUseCase;
import com.tasks.domain.ports.out.TaskRepositoryPort;
import com.tasks.infrastructure.repositories.TaskRepositoryAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {

    @Bean
    public TaskService taskService(CreateTaskUseCase createTaskUseCase, GetTaskUseCase getTaskUseCase) {
        return new TaskService(createTaskUseCase, getTaskUseCase);
    }

    @Bean
    public TaskRepositoryPort taskRepositoryPort(TaskRepositoryAdapter taskRepositoryAdapter) {
        return taskRepositoryAdapter;
    }
}
