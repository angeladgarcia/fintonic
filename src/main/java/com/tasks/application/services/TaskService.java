package com.tasks.application.services;

import com.tasks.domain.model.Task;
import com.tasks.domain.ports.in.CreateTaskUseCase;
import com.tasks.domain.ports.in.GetTaskUseCase;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Objects;

@Service
public class TaskService implements CreateTaskUseCase, GetTaskUseCase {

    private final CreateTaskUseCase createTaskUseCase;
    private final GetTaskUseCase getTaskUseCase;

    public TaskService(CreateTaskUseCase createTaskUseCase, GetTaskUseCase getTaskUseCase) {
        this.createTaskUseCase = Objects.requireNonNull(createTaskUseCase, "CreateTaskUseCase must not be null");
        this.getTaskUseCase = Objects.requireNonNull(getTaskUseCase, "GetTaskUseCase must not be null");
    }

    @Override
    public Task createTask(Task task) {
        return createTaskUseCase.createTask(task);
    }

    @Override
    public Optional<Task> getTaskById(String id) {
        return getTaskUseCase.getTaskById(id);
    }

    @Override
    public List<Task> getAllTasks() {
        return getTaskUseCase.getAllTasks();
    }
}