package com.tasks.application.usecases;

import com.tasks.domain.model.Task;
import com.tasks.domain.ports.in.GetTaskUseCase;
import com.tasks.domain.ports.out.TaskRepositoryPort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Objects;

@Service
public class GetTaskUseCaseImpl implements GetTaskUseCase {

    private final TaskRepositoryPort taskRepositoryPort;

    public GetTaskUseCaseImpl(TaskRepositoryPort taskRepositoryPort) {
        this.taskRepositoryPort = Objects.requireNonNull(taskRepositoryPort, "TaskRepositoryPort must not be null");
    }

    @Override
    public Optional<Task> getTaskById(String id) {
        return taskRepositoryPort.findById(id);
    }

    @Override
    public List<Task> getAllTasks() {
        return taskRepositoryPort.findAll();
    }
}
