package com.tasks.application.usecases;

import com.tasks.domain.model.Task;
import com.tasks.domain.ports.in.CreateTaskUseCase;
import com.tasks.domain.ports.out.TaskRepositoryPort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Service
public class CreateTaskUseCaseImpl implements CreateTaskUseCase {

    private final TaskRepositoryPort taskRepositoryPort;

    public CreateTaskUseCaseImpl(TaskRepositoryPort taskRepositoryPort) {
        this.taskRepositoryPort = Objects.requireNonNull(taskRepositoryPort, "TaskRepositoryPort must not be null");
    }

    @Override
    @Transactional
    public Task createTask(Task task) {
        return taskRepositoryPort.save(task);
    }
}