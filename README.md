# Fintonic

# Pruebas

Ejecutar las pruebas desde la línea de comandos:

    Navega hasta el directorio raíz de tu proyecto desde la línea de comandos.
    Ejecuta el siguiente comando para ejecutar todas las pruebas:
    mvn test

Ejecuta el siguiente comando para ejecutar todas las pruebas de aceptación:
    mvn verify
    
# Postman
Método obtener listado de tareas:
GET Ruta:http://localhost:8080/api/tasks
[
    {
        "id": "6655bf75450088657534500d",
        "title": "Tarea de ejemplo",
        "description": "Esta es una tarea de ejemplo",
        "creationDate": "2022-05-30T12:00:00",
        "completed": false
    }
]
Método crear tarea: 
POST Ruta: http://localhost:8080/api/tasks Input: JSON REQUEST
{
    "title": "Tarea de ejemplo",
    "description": "Esta es una tarea de ejemplo",
    "creationDate": "2022-05-30T12:00:00",
    "completed": false
}

JSON RESPONSE
{
    "id": "6655bf75450088657534500d",
    "title": "Tarea de ejemplo",
    "description": "Esta es una tarea de ejemplo",
    "creationDate": "2022-05-30T12:00:00",
    "completed": false
}
